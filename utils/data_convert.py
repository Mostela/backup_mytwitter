import json

from model.post import Post
import base64


class DataConvert:
    """
    convert data in fields for base64 and export this object in format json
    :return str
    """
    def convert(self, data: Post) -> str:
        for key, value in zip(vars(data), data.__dict__.values()):
            setattr(data, key, self.encrypt(value))
        return json.dumps(data.__dict__)

    def encrypt(self, field_str: str) -> str:
        return base64.b64encode(f"{field_str}".encode('utf-8')).decode('utf-8')

    def decrypt(self, field_str: str):
        return base64.b64decode(field_str).decode()

    """
    deconvert data in object type Post and return your fields in a new state
    :return Post
    """
    def deconvert(self, json_data: str) -> Post:
        data = Post(None)
        data_raw = json.loads(json_data)
        for key in data_raw.keys():
            setattr(data, key, self.decrypt(data_raw[key]))
        return data
