import os

import pymongo

from hook import *
from model import *
from utils import *

if __name__ == '__main__':
    data_convert = DataConvert()
    file = FileControl()

    mongo_father = Mongodb(
        user=os.getenv('MONGO_USER'),
        database=os.getenv('MONGO_DB'),
        password=os.getenv('MONGO_PASS')
    )

    for item in mongo_father.connect()[os.getenv('MONGO_COLLECTION')].find():
        if item and type(item) is dict:
            data = Post(None)
            for value in item.keys():
                setattr(data, value, item[value])
            file.add_buffer(data_convert.convert(data))
    if file.save("."):
        print("Finish no errors")
