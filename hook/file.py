import datetime
import os


class FileControl:
    _buffer = []

    def save(self, dirname: str) -> bool:
        try:
            dt = datetime.datetime.now().strftime("%y%m%d%H%M")
            filename = f"{dirname}/{dt}_{os.getenv('MONGO_COLLECTION')}_{os.getenv('MONGO_DB')}.txt"

            print(f"{filename} have {len(self._buffer)} rows")

            file = open(filename, "w")
            for value in self._buffer:
                file.write(f"{value}\n")
            file.close()
            self._buffer.clear()
            return True
        except Exception as ex:
            print(ex)
            return False

    def add_buffer(self, data: str):
        if type(data) is not str:
            raise TypeError("Invalid type for add in buffer")
        self._buffer.append(data)

    def read(self, dirname: str) -> str:
        pass
