from pymongo.errors import PyMongoError, ProtocolError, CollectionInvalid, NetworkTimeout, InvalidId, OperationFailure
from pymongo import MongoClient


class Mongodb:
    def __init__(self, user, password, database):
        self._user = user
        self._password = password
        self._database = database

    def connect(self):
        try:
            client = MongoClient(
                f"mongodb+srv://{self._user}:{self._password}@{self._database}.6nldc.mongodb.net/{self._database}?retryWrites=true&w=majority")
            return client[self._database]
        except (PyMongoError, ProtocolError) as puy:
            print("Problem in PyMongo", str(puy))
        except CollectionInvalid as ci:
            print("Collection invalid", str(ci))
        except Exception as ex:
            print(ex)
