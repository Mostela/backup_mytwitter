import datetime


class Post:
    def __init__(self, nome, image: str = None, visualizacao: int = 0):
        self.nome = nome
        self.visualizacao = visualizacao
        self.date_created = datetime.datetime.now().__str__()
        self.image = image
        self.date_last_view = None

    @property
    def _nome(self):
        return self.nome

    @_nome.setter
    def _nome(self, value):
        self.nome = value

    @property
    def _visualizacao(self):
        return self.visualizacao

    @_visualizacao.setter
    def _visualizacao(self, value):
        self.visualizacao = value

    @property
    def _date_last_view(self):
        return self.date_last_view

    @_date_last_view.setter
    def _date_last_view(self, value):
        self._date_last_view = value
